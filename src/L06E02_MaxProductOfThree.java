import java.lang.reflect.Array;
import java.util.Arrays;


public class L06E02_MaxProductOfThree {

	public static void main(String[] args) {
		
		//System.out.println(Arrays.toString(Solution.solution(new int[] {2,3,1,5})));
		System.out.println(Solution.solution(new int[] {2,1,1,2,3,1}));
		
		//System.out.println(Solution.solution(new int[] {2,3,1,5,4,7,8,9,10,11}));
		//System.out.println(Solution.solution(new int[] {}));
		//System.out.println(Solution.solution(new int[] {2,3,4,5,6,7,8,9,10,11}));


	}


	static class Solution {
		static int solution(int[] A) {

			// main idea: 
	        // max_1 = positive * positive * positive
	        // max_2 = negative * negative * positive
	        // max = Math.max(max_1, max_1)
	        // just need to sort the integer array
	        
	        // sort the array
	        Arrays.sort(A);

	        // max_1 = 1st biggest * 2nd biggest * 3rd biggest 
	        int max_1 = A[A.length-1] * A[A.length-2] * A[A.length-3];
	        
	        // max_2 = 1st smallest * 2nd smallest * 1st biggest
	        int max_2 = A[0] * A[1] * A[A.length-1];

	        // take the maximum        
	        int max = Math.max(max_1, max_2);
	        
	        return max;		}
		
	}

}
