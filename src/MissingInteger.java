import java.util.Arrays;

public class MissingInteger {

	public static void main(String[] args) {
		
		System.out.println(Solution.solution(new int[] {1, 0, 1, 0, 1, 99, 88, 2}));

	}
	
	static class Solution {
		static int solution(int[] A) {
	        Arrays.sort(A);
	        
	        int min = 0;
	        int last =  A[0];
	        int max = A[A.length - 1];
	        
	        int p = 0;
	        int i = 0;
	        while (p == 0 ) {
				if (A[i] >= 0) {
					p = i;
				}
				i++;
			}
	        
	        for (i=0; i < A.length; i++){
	            if (A[i] < max) {
	                if ((A[i] != last + 1) && A[i] > 0 ) {
	                    if ( min < A[i] ) min = A[i];
	                } else  {
	                    if ( min < A[i] ) min = A[i];
	                }
	            } else {
	                if ( min < A[i]) min = A[i];
	            }
	            
	            if (A[i] < 0) min = 0;
	            last = A[i];
	        }
	        
	        min += 1;
	        return min;

		}
		
	}
}
