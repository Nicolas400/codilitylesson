import java.lang.reflect.Array;
import java.util.Arrays;


public class L05E04_PassingCars {

	public static void main(String[] args) {
		
		//System.out.println(Arrays.toString(Solution.solution(new int[] {2,3,1,5})));
		System.out.println(Solution.solution(new int[] {0,1,0,1,1}));
		
		//System.out.println(Solution.solution(new int[] {2,3,1,5,4,7,8,9,10,11}));
		//System.out.println(Solution.solution(new int[] {}));
		//System.out.println(Solution.solution(new int[] {2,3,4,5,6,7,8,9,10,11}));


	}

	//Solution.
	//https://app.codility.com/demo/results/trainingBN8XTH-2GF/
	
	static class Solution {
		static int solution(int[] A) {
			
			if (A.length == 0 )
				return -1;

			// limit on number of passing cars (given in problem description) (1,000,000,000)
			final int CAR_LIMIT = 1000000000;
	        int passingCars = 0;

	        int carsTravelingEast = 0;
	        int N = A.length;
		 
	        for (int i = 0; i < N; i++) {
	        	if (A[i] == 0) {
	        		carsTravelingEast++;
	        	}
	        	//Por cada 1, hay que sumar todos los 0 anteriores...
	        	if (A[i] == 1 && carsTravelingEast > 0) {
	        		passingCars += carsTravelingEast;
	        		// if passing cars exceeds limit set by problem description
	        		if (passingCars > CAR_LIMIT)
	                	    return -1;
	        	}
	        }                   
			return passingCars;

		}
		
	}
}
