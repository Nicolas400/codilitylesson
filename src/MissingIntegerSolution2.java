import java.util.Arrays;

public class MissingIntegerSolution2 {

	public static void main(String[] args) {
		
		System.out.println(Solution.solution(new int[] {1, 3, 6, 4, 1, 2}));

	}
	
	static class Solution {
		static int solution(int[] A) {
			int smallest = 1;
	        Arrays.sort(A);
	        for (int i = 0; i < A.length; i++) {
	            if (A[i] == smallest) {
	                smallest++;
	            }
	        }
	        return smallest;
		}
		
	}
}
