import java.lang.reflect.Array;
import java.util.Arrays;

public class L03E02_PermMissingElem {

	public static void main(String[] args) {
		
		//System.out.println(Arrays.toString(Solution.solution(new int[] {2,3,1,5})));
		System.out.println(Solution.solution(new int[] {2,3,1,5}));
		
		System.out.println(Solution.solution(new int[] {2,3,1,5,4,7,8,9,10,11}));
		System.out.println(Solution.solution(new int[] {}));
		System.out.println(Solution.solution(new int[] {2,3,4,5,6,7,8,9,10,11}));


	}

	//Solution.
	//https://app.codility.com/demo/results/trainingBN8XTH-2GF/
	
	static class Solution {
		static int solution(int[] A) {

	
			if (A.length == 0 )
				return 1;
//			if (A.length > 0 && A[0] != 1))
	
			long n = A.length + 1;
			long res = n * (n + 1) / 2;
			
	        for (int element : A)
	        	res -= element;
	        
//			Arrays.sort(A);
//			System.out.println(Arrays.toString(A));
//			
////			if(A[A.length-1] > A[0] + A.length - 1) {
////				return A[A.length-1] - 1;
////			}
//			
//			for (int i = 0; i < A.length-1; i++) {
//				if (A[i] != A[i+1] - 1) {
//					return A[i] + 1;
//				}
//			}
//	    	return A[0];
	        return (int)res;
		}
	}
}
