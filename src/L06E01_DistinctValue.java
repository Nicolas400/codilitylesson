import java.lang.reflect.Array;
import java.util.Arrays;


public class L06E01_DistinctValue {

	public static void main(String[] args) {
		
		//System.out.println(Arrays.toString(Solution.solution(new int[] {2,3,1,5})));
		System.out.println(Solution.solution(new int[] {2,1,1,2,3,1}));
		
		System.out.println(SolutionB.solution(new int[] {2,1,1,2,3,1}));
		System.out.println(SolutionB.solution(new int[] {}));
		
		//System.out.println(Solution.solution(new int[] {2,3,1,5,4,7,8,9,10,11}));
		//System.out.println(Solution.solution(new int[] {}));
		//System.out.println(Solution.solution(new int[] {2,3,4,5,6,7,8,9,10,11}));


	}


	static class Solution {
		static int solution(int[] A) {
			int count = 0;
			Arrays.sort(A);
			
			int max = A[A.length-1]; 
			
			int sum[] = new int[max+1];
			for (int i = 0; i < A.length; i++) {
				sum[A[i]]++;
			}

			for (int i = 0; i < sum.length; i++) {
				if (sum[i] > 0) {
					count++;
				}
			}
			return count;
		}
		
	}

	// 100 x 100 !
	static class SolutionB {
		static int solution(int[] A) {
			int count = 0;
			if (A.length <= 0) return 0;
			
			Arrays.sort(A);
			
			
			int prev = A[0]-1; 
			
			for (int i = 0; i < A.length; i++) {
				if (A[i] != prev) {
					prev = A[i];
					count++;
				}
			}

			return count;
		}
		
	}
}
