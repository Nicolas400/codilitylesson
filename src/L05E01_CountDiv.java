import java.lang.reflect.Array;
import java.util.Arrays;

public class L05E01_CountDiv {

	public static void main(String[] args) {
		
		//System.out.println(Arrays.toString(Solution.solution(new int[] {2,3,1,5})));
		System.out.println(Solution.solution(6, 11, 2));

		
		//System.out.println(Solution.solution(new int[] {2,3,1,5,4,7,8,9,10,11}));
		//System.out.println(Solution.solution(new int[] {}));
		//System.out.println(Solution.solution(new int[] {2,3,4,5,6,7,8,9,10,11}));


	}

	//Solution.
	//https://app.codility.com/demo/results/trainingBN8XTH-2GF/
	
	static class Solution {
		static int solution(int A, int B, int K) {
			
			int b_k = B / K;
			int a_k = A / K;
			
			//return b / k - a / k + (a % k == 0 ? 1 : 0);
			
			int count = 0;
	        for (int i = A; i <= B; i++) {
	        	if (i % K == 0 ) {
	        		count++;
	        	}
	        }                   
			return count;
	
		}
	}
}
