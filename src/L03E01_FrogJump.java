import java.util.Arrays;

public class L03E01_FrogJump {

	public static void main(String[] args) {
		
		System.out.println(Solution.solution(10, 85, 30));
		
		System.out.println(Solution.solution(1, 5, 2));

		System.out.println(Solution.solution(1, 5, 4));

		System.out.println(Solution.solution(10, 10, 2));

	}

	//Solution.
	
	static class Solution {
		static int solution(int X, int Y, int D) {
			
			if (Y==X)
				return 0;

			int r = (Y - X - 1) / D;
	    	return r + 1;
		}
	}
}
