import java.util.Arrays;

public class L02E01_CyclicRotation {

	public static void main(String[] args) {
		
		System.out.println(Arrays.toString(Solution.solution(new int[] {3, 8, 9, 7, 6}, 3)));
		
		System.out.println(Arrays.toString(Solution.solution(new int[] {}, 3)));
		//System.out.println(Solution.solution(529));
		//System.out.println(Solution.solution(20));
		//System.out.println(Solution.solution(15));

	}

	//Solution.
	
	static class Solution {
		static int[] solution(int[] A, int K) {

		
			// /while (K >= A.length && A.length > 0) {
			if (K >= A.length && A.length > 0) {
				K = K % A.length;
				//K = K - A.length;
			}
			
			if (K == 0 || A.length < 2)
				return A;
			
			//copia Elemento a arreglo auxiliar
			int[] moves = Arrays.copyOfRange(A, A.length - K, A.length);

			int pos_f = A.length - 1;
			for (int i = A.length - moves.length - 1; i >= 0; i--) {
				A[pos_f] = A[i];
				pos_f--;
			}
					
			for (int i = 0; i < moves.length; i++) {
				A[i] = moves[i];
			}

	        return A;
		}
	}
}
