import java.util.Arrays;

public class L02E02_OddOccurency {

	public static void main(String[] args) {
		
		System.out.println(Solution.solution(new int[] {9, 3, 9, 3, 9, 7, 9}));
		

	}

	//Solution.
	
	static class Solution {
		static int solution(int[] A) {

		
	    	Arrays.sort(A);
	    	
	    	for (int i = 0; i < A.length; i++) {
	    		if (i < A.length-2 && A[i] != A[i+1])
	    			return A[i];
	    		i++;
	    	}
	    	
	    	return A[A.length-1];
		}
	}
}
