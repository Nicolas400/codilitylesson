

public class L01E01_BinaryGap {

	public static void main(String[] args) {
		
		System.out.println(Solution.solution(9));
		System.out.println(Solution.solution(529));
		System.out.println(Solution.solution(20));
		System.out.println(Solution.solution(15));

	}

	static class Solution {
		static int solution(int N) {
			
			int count = 0, max = 0;

		    //Mueve los bits a la derecha, todos los pares terminan en 0
		    while (N % 2 == 0) {
		    	N = N >> 1;
		    }

	    	while (N > 0) {
	    		String s = Integer.toBinaryString(N);
	    	    // System.out.println("numero  = " + Integer.toBinaryString(N));
	    	    if (s.endsWith("1")) {
	    	    	if (count > max)
		    			max = count;
	    	    	count = 0;
	    	    } else {
	    	    	count++;
	    	    }
		    	N = N >> 1;
	    	}

	    	// System.out.println("gap  = " + max);

	        return max;
			
			
		}
	}
}
